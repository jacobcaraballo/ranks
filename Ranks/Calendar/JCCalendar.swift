//
//  JCCalendarPageViewController.swift
//  JCCalendarView
//
//  Created by Jacob Caraballo on 7/16/18.
//  Copyright © 2018 Jacob Caraballo. All rights reserved.
//

import Foundation
import UIKit

struct JCCalendarColorScheme {
	var text: UIColor = .black
	var today: UIColor = .red
	var selection: UIColor = .black
	var selectionText: UIColor = .white
	
	init() { }
}

enum JCDirection {
	case none, backwards, forwards
}

protocol JCCalendarDelegate {
	func calendar(_ calendar: JCCalendar, didSelectDate date: Date, selectedAutomatically: Bool, isReselecting: Bool)
	func calendar(_ calendar: JCCalendar, willUpdateHeight height: CGFloat)
}

protocol JCCalendarDataSource {
	func calendar(_ calendar: JCCalendar, markerColorForDate date: Date) -> UIColor?
}

struct JCCalendarStyle {
	
	var headerHeight: CGFloat = 88
	var dayHeight: CGFloat = 0
	
}

class JCCalendar: UIView {
	
	var style: JCCalendarStyle
	var delegate: JCCalendarDelegate?
	var dataSource: JCCalendarDataSource?
	var collectionView: UICollectionView {
		return page2.collectionView
	}
	var dateFormat = "MMMM yyyy" {
		didSet {
			setDateViewLabelDate(date)
		}
	}
	
	private var date: Date {
		didSet {
			guard date != oldValue else { return }
			resetPageDates()
		}
	}
	
	
	private let colorScheme = JCCalendarColorScheme()
	private let scrollView = UIScrollView()
	private let weekView = UIView()
	private let dateView = UIView()
	private let dateViewLabel = UILabel()
	private let todayButton = UIButton()
	private var selectingDate: Date?
	private var targetPage: Int?
	private var heightPrepared = false
	private var selectedPageAutomatically = true
	
	private let page1: JCCalendarPage
	private let page2: JCCalendarPage
	private let page3: JCCalendarPage
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	init(date: Date = Date(), style: JCCalendarStyle = JCCalendarStyle()) {
		
		self.style = style
		self.date = date
		
		
		// setup next month
		var dateAfter = date.adding(month: 1)
		if dateAfter.hasSameMonth(asDate: Date()) {
			dateAfter = Date()
		} else {
			dateAfter = dateAfter.beginningOfMonth
		}
		
		
		// setup previous month
		var dateBefore = date.adding(month: -1)
		if dateBefore.hasSameMonth(asDate: Date()) {
			dateBefore = Date()
		} else {
			dateBefore = dateBefore.beginningOfMonth
		}
		
		
		// create pages
		page1 = JCCalendarPage(date: dateBefore, style: style)
		page2 = JCCalendarPage(date: date, style: style)
		page3 = JCCalendarPage(date: dateAfter, style: style)
				
		
		super.init(frame: .zero)
		
		backgroundColor = .white
		
		
		// setup page delegates
		page1.delegate = self
		page2.delegate = self
		page3.delegate = self
		
		
		// setup page data sources
		page1.dataSource = self
		page2.dataSource = self
		page3.dataSource = self
		
		
		// layout our calendar and pages
		layoutDateView()
		layoutWeekView()
		layoutScrollView()
		addPages()
		
	}
	
	override func willMove(toWindow newWindow: UIWindow?) {
		super.willMove(toWindow: newWindow)
		
		if let _ = newWindow {
			
			scrollView.layoutIfNeeded()
			scrollView.contentSize = CGSize(width: scrollView.frame.size.width * (3), height: scrollView.frame.size.height)
			goToPage(1)
			
		}
		
	}
	
	private func layoutScrollView() {
		
		scrollView.delegate = self
		scrollView.panGestureRecognizer.maximumNumberOfTouches = 1
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		scrollView.isPagingEnabled = true
		scrollView.bounces = false
		scrollView.showsHorizontalScrollIndicator = false
		addSubview(scrollView)
		
		NSLayoutConstraint.activate([
			
			scrollView.widthAnchor.constraint(equalTo: widthAnchor),
			scrollView.topAnchor.constraint(equalTo: weekView.bottomAnchor),
			scrollView.centerXAnchor.constraint(equalTo: centerXAnchor),
			scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
			
			])
		
	}
	
	private func layoutDateView() {
		
		// layout dateview
		dateView.translatesAutoresizingMaskIntoConstraints = false
		addSubview(dateView)
		
		NSLayoutConstraint.activate([
			
			dateView.widthAnchor.constraint(equalTo: widthAnchor),
			dateView.heightAnchor.constraint(equalToConstant: style.headerHeight / 2),
			dateView.topAnchor.constraint(equalTo: topAnchor),
			dateView.centerXAnchor.constraint(equalTo: centerXAnchor)
			
			])
		
		
		// layout label
		dateViewLabel.translatesAutoresizingMaskIntoConstraints = false
		setDateViewLabelDate(date)
		dateViewLabel.font = UIFont.systemFont(ofSize: 22)
		dateViewLabel.textAlignment = .center
		dateView.addSubview(dateViewLabel)
		
		NSLayoutConstraint.activate([
			
			dateViewLabel.widthAnchor.constraint(equalTo: dateView.widthAnchor),
			dateViewLabel.heightAnchor.constraint(equalTo: dateView.heightAnchor),
			dateViewLabel.centerXAnchor.constraint(equalTo: dateView.centerXAnchor),
			dateViewLabel.centerYAnchor.constraint(equalTo: dateView.centerYAnchor)
			
			])
		
		
		todayButton.translatesAutoresizingMaskIntoConstraints = false
		todayButton.setTitle("Today", for: .normal)
		todayButton.setTitleColor(colorScheme.today, for: .normal)
		todayButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
		todayButton.sizeToFit()
		
		todayButton.addTarget(self, action: #selector(todayButtonPressed(_:)), for: [.touchUpInside])
		todayButton.addTarget(self, action: #selector(todayButtonExit(_:)), for: [.touchDragExit, .touchCancel])
		todayButton.addTarget(self, action: #selector(todayButtonDown(_:)), for: [.touchDown, .touchDragEnter])
		
		dateView.addSubview(todayButton)
		
		NSLayoutConstraint.activate([
			
			todayButton.trailingAnchor.constraint(equalTo: dateView.trailingAnchor, constant: -20),
			todayButton.centerYAnchor.constraint(equalTo: dateView.centerYAnchor)
			
			])
		
		
	}
	
	private func layoutWeekView() {
		
		weekView.translatesAutoresizingMaskIntoConstraints = false
		addSubview(weekView)
		
		NSLayoutConstraint.activate([
			
			weekView.widthAnchor.constraint(equalTo: widthAnchor),
			weekView.heightAnchor.constraint(equalToConstant: style.headerHeight / 2),
			weekView.centerXAnchor.constraint(equalTo: centerXAnchor),
			weekView.topAnchor.constraint(equalTo: dateView.bottomAnchor)
			
			])
		
		var previousWeekdayView: UIView?
		for i in 0..<7 {
			
			let weekdayView = UIView()
			weekdayView.translatesAutoresizingMaskIntoConstraints = false
			weekView.addSubview(weekdayView)
			
			NSLayoutConstraint.activate([
				
				weekdayView.widthAnchor.constraint(equalTo: weekView.widthAnchor, multiplier: 1 / 7),
				weekdayView.heightAnchor.constraint(equalTo: weekView.heightAnchor),
				weekdayView.leadingAnchor.constraint(equalTo: previousWeekdayView?.trailingAnchor ?? weekView.leadingAnchor),
				weekdayView.bottomAnchor.constraint(equalTo: weekView.bottomAnchor)
				
				])
			
			previousWeekdayView = weekdayView
			
			
			// setup label for weekday view
			let textLabel = UILabel()
			textLabel.translatesAutoresizingMaskIntoConstraints = false
			textLabel.textAlignment = .center
			textLabel.text = Date.veryShortWeekdaySymbols[i]
			
			
			
			// set text color for weekends
			if i == 0 || i == 6 {
				textLabel.textColor = UIColor(white: 0, alpha: 0.5)
			}
			
			
			// add to weekday view
			weekdayView.addSubview(textLabel)
			
			
			// setup constraints
			NSLayoutConstraint.activate([
				
				textLabel.widthAnchor.constraint(equalTo: weekdayView.widthAnchor),
				textLabel.heightAnchor.constraint(equalTo: weekdayView.heightAnchor),
				textLabel.centerXAnchor.constraint(equalTo: weekdayView.centerXAnchor),
				textLabel.centerYAnchor.constraint(equalTo: weekdayView.centerYAnchor)
				
				])
			
		}
		
	}
	
	private func addPages() {
		
		// page 1
		page1.translatesAutoresizingMaskIntoConstraints = false
		scrollView.addSubview(page1)
		
		NSLayoutConstraint.activate([
			
			page1.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			page1.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
			page1.topAnchor.constraint(equalTo: scrollView.topAnchor),
			page1.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
			
			])
		
		
		// page 2
		page2.translatesAutoresizingMaskIntoConstraints = false
		scrollView.addSubview(page2)
		
		NSLayoutConstraint.activate([
			
			page2.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			page2.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
			page2.topAnchor.constraint(equalTo: scrollView.topAnchor),
			page2.leadingAnchor.constraint(equalTo: page1.trailingAnchor),
			
			])
		
		
		// page 3
		page3.translatesAutoresizingMaskIntoConstraints = false
		scrollView.addSubview(page3)
		
		NSLayoutConstraint.activate([
			
			page3.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
			page3.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
			page3.topAnchor.constraint(equalTo: scrollView.topAnchor),
			page3.leadingAnchor.constraint(equalTo: page2.trailingAnchor),
			
			])
		
	}
	
	private func resetPageDates() {
		page1.setDate(toMonthBeforeDate: date)
		page2.setDate(date)
		page3.setDate(toMonthAfterDate: date)
		
		page2.delegate?.calendarPage(page2, didSelectDate: date, selectedAutomatically: selectedPageAutomatically, isReselecting: false)
		selectedPageAutomatically = true
		
		setDateViewLabelDate(page2.date)
	}
	
	private func prepareForPageChange(direction: JCDirection) {
		let offset = direction == .backwards ? -1 : 1
		var date = self.date.adding(month: offset)
		if date.hasSameMonth(asDate: Date()) {
			date = Date()
		} else {
			date = date.beginningOfMonth
		}
		self.date = date
		updateContentOffsetForMovement(inDirection: direction)
	}
	
	private func prepareHeightForPageChange(direction: JCDirection, forceDate: Date?) {
		
		let offset = direction == .backwards ? -1 : 1
		var date = forceDate ?? self.date.adding(month: offset)
		if date.hasSameMonth(asDate: Date()) {
			date = Date()
		} else {
			date = date.beginningOfMonth
		}
		let numWeeks = date.numberOfWeeksInMonth
		var dayHeight = (frame.width / 7).rounded(.down)
		if style.dayHeight > 0 {
			dayHeight = style.dayHeight
		}
		delegate?.calendar(self, willUpdateHeight: dayHeight * CGFloat(numWeeks) + style.headerHeight)
		
	}
	
	private func goToPage(_ page: Int, animated: Bool = false) {
		
		scrollView.isUserInteractionEnabled = !animated
		
		var offset = scrollView.contentOffset
		let x = scrollView.frame.size.width * CGFloat(page)
		
		if offset.x != x {
			offset.x = x
			scrollView.setContentOffset(offset, animated: animated)
		}
		
	}
	
	private func pageAtOffset(_ offset: CGPoint) -> Int {
		return Int(offset.x / scrollView.frame.size.width)
	}
	
	
	private func updateContentOffsetForMovement(inDirection direction: JCDirection) {
		
		switch direction {
			
		case .backwards:
			scrollView.contentOffset.x += scrollView.frame.size.width
			
		case .forwards:
			scrollView.contentOffset.x -= scrollView.frame.size.width
			
		default:
			break
			
		}
		
	}
	
	func selectDate(_ date: Date) {
		
		selectingDate = date
		
		if date.hasSameMonth(asDate: self.date) {
			page2.setDate(date)
		} else {
			
			let i = date < self.date ? 0 : 2
			let page = i == 0 ? page1 : page3
			
			page.setDate(date, forceUpdateLayout: true)
			
			goToPage(i, animated: true)
			scrollViewWillEndAtPage(i, forceDate: date)
			
		}
		
	}
	
	private func setDateViewLabelDate(_ date: Date) {
		dateViewLabel.text = date.string(format: dateFormat)
		
		if date.hasSameMonth(asDate: Date()) {
			dateViewLabel.textColor = colorScheme.today
			todayButton.isHidden = true
		} else {
			dateViewLabel.textColor = colorScheme.text
			todayButton.isHidden = false
		}
	}
	
	@objc private func todayButtonDown(_ sender: UIButton) {
		sender.alpha = 0.4
	}
	@objc private func todayButtonExit(_ sender: UIButton) {
		sender.alpha = 1
	}
	@objc private func todayButtonPressed(_ sender: UIButton) {
		sender.alpha = 1
		
		let today = Date()
		selectDate(today)
	}
	
}



extension JCCalendar: UIScrollViewDelegate, JCCalendarPageDelegate, JCCalendarPageDataSource {
	
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		scrollView.isUserInteractionEnabled = false
		targetPage = Int(targetContentOffset.pointee.x / scrollView.frame.size.width)
		
		if targetPage == 1 {
			targetPage = nil
			scrollView.isUserInteractionEnabled = true
		} else {
			scrollViewWillEndAtPage(targetPage!, forceDate: nil)
		}
		
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		guard let targetPage = targetPage else { return }
		scrollViewDidEndAtPage(targetPage)
		scrollView.isUserInteractionEnabled = true
		self.targetPage = nil
	}
	
	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
		
		guard let date = selectingDate else { return }
		
		goToPage(1)
		self.date = date
		
		selectingDate = nil
		scrollView.isUserInteractionEnabled = true
		
	}
	
	private func scrollViewDidEndAtPage(_ page: Int) {
		prepareForPageChange(direction: page < 1 ? .backwards : .forwards)
	}
	
	private func scrollViewWillEndAtPage(_ page: Int, forceDate: Date?) {
		prepareHeightForPageChange(direction: page < 1 ? .backwards : .forwards, forceDate: forceDate)
	}
	
	func calendarPage(_ page: JCCalendarPage, didSelectDate date: Date, selectedAutomatically: Bool, isReselecting: Bool) {
		
		if self.date.hasSameMonth(asDate: date) {
			delegate?.calendar(self, didSelectDate: date, selectedAutomatically: selectedAutomatically, isReselecting: isReselecting)
		} else {
			selectedPageAutomatically = selectedAutomatically
			selectDate(date)
		}
		
	}
	
	func calendarPage(_ calendarPage: JCCalendarPage, markerColorForDate date: Date) -> UIColor? {
		return dataSource?.calendar(self, markerColorForDate: date)
	}
	
	func calendarPage(_ page: JCCalendarPage, willUpdateHeight height: CGFloat) {
		guard !heightPrepared else { return }
		if page == page2 {
			delegate?.calendar(self, willUpdateHeight: height + style.headerHeight)
			heightPrepared = true
		}
	}
	
}
