//
//  StatusBarBackground.swift
//  Money
//
//  Created by Jacob Caraballo on 5/7/17.
//  Copyright © 2017 Jacob Caraballo. All rights reserved.
//

import Foundation
import UIKit

var statusBars = [StatusBarBackground]()
class StatusBarBackground: UIVisualEffectView {
	
	static let height = UIApplication.shared.statusBarFrame.size.height
	private var heightConstraint: NSLayoutConstraint!
	
	init() {
		super.init(effect: UIBlurEffect(style: .light))
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	class func add(toView view: UIView) {
		let statusBarBG = StatusBarBackground()
		statusBarBG.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(statusBarBG)
		
		statusBarBG.heightConstraint = statusBarBG.heightAnchor.constraint(equalToConstant: UIApplication.shared.statusBarFrame.size.height)
		NSLayoutConstraint.activate([
			
			statusBarBG.widthAnchor.constraint(equalTo: view.widthAnchor),
			statusBarBG.heightConstraint,
			statusBarBG.topAnchor.constraint(equalTo: view.topAnchor),
			statusBarBG.centerXAnchor.constraint(equalTo: view.centerXAnchor)
			
			])
		statusBars.append(statusBarBG)
	}
	
	func toggle(visible: Bool) {
		heightConstraint.constant = visible ? StatusBarBackground.height : 0
		UIView.animate(withDuration: 0.2) {
			self.layoutIfNeeded()
		}
	}
	
	deinit {
		if let i = statusBars.index(of: self) {
			statusBars.remove(at: i)
		}
	}
	
}
