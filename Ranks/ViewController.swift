//
//  ViewController.swift
//  Ranks
//
//  Created by Jacob Caraballo on 8/28/18.
//  Copyright © 2018 Jacob Caraballo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, JCCalendarDelegate, JCCalendarDataSource, UITableViewDelegate, UITableViewDataSource {
	
	var scrollView = SectionScrollView()
	var tableView = UITableView(frame: .zero, style: .grouped)
	var tvSection: SSViewSection!
	var calendarSection: SSViewSection!
	
	var data = [
	
		[
			"Quiz 1",
			"Project Milestone 1",
			"Quiz 2",
			"Project Milestone 2"
		],
		[
			"Exam 1",
			"Proposal",
			"Quiz 3",
			"Midterm",
			"Final"
		]
	
	]

	override func viewDidLoad() {
		super.viewDidLoad()
		layoutScrollView()
		layoutCalendarView()
		layoutTableView()
		
		StatusBarBackground.add(toView: view)
	}
	
	func layoutCalendarView() {
		
		var cStyle = JCCalendarStyle()
		cStyle.headerHeight = 70
		
		let calendar = JCCalendar(date: Date(), style: cStyle)
		calendar.backgroundColor = .white
		calendar.delegate = self
		calendar.dataSource = self
		
		let style = SSViewSectionStyle()
		style.hasShadow = true
		style.hasBorder = false
		style.insets = UIEdgeInsets(horizontal: 0)
		style.cornerRadius = 0
		calendarSection = scrollView.add(calendar, style: style)
		
	}
	
	func layoutScrollView() {
		
		scrollView.translatesAutoresizingMaskIntoConstraints = false
		scrollView.showsVerticalScrollIndicator = false
		view.addSubview(scrollView)
		
		NSLayoutConstraint.activate([
			
			scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
			scrollView.heightAnchor.constraint(equalTo: view.heightAnchor),
			scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			scrollView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
			
			])
		
	}
	
	func layoutTableView() {
		
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.delegate = self
		tableView.dataSource = self
		tableView.tintColor = UIColor(white: 0.15, alpha: 1)
		tableView.backgroundColor = UIColor.clear
		tableView.rowHeight = 70
		tableView.showsVerticalScrollIndicator = false
		tableView.scrollsToTop = true
		tableView.bounces = false
		
		let style = SSViewSectionStyle()
		style.hasShadow = false
		style.hasBorder = false
		style.insets = UIEdgeInsets(horizontal: 0)
		style.cornerRadius = 0
		tvSection = scrollView.add(tableView, style: style)
		tableView.contentSizeDidChange = { _ in
			self.tvSection.setHeight(
				self.tableView.contentSize.height,
				animated: false
			)
		}
		
	}
	
	func calendar(_ calendar: JCCalendar, didSelectDate date: Date, selectedAutomatically: Bool, isReselecting: Bool) {
		guard !isReselecting else { return }
		print(date)
	}
	
	func calendar(_ calendar: JCCalendar, markerColorForDate date: Date) -> UIColor? {
		return nil
	}
	
	func calendar(_ calendar: JCCalendar, willUpdateHeight height: CGFloat) {
		
		calendarSection.setHeight(height, animated: true)
		
		/*
		let constant = UIApplication.shared.statusBarFrame.height + calendar.headerHeight + height
		guard transactionTopConstraint.constant != constant else { return }
		
		let animate = transactionTopConstraint.constant != 0
		
		self.view.layoutIfNeeded()
		transactionTopConstraint.constant = constant
		
		if animate {
			let anim = UIViewPropertyAnimator(duration: 0.3, curve: .linear) {
				self.view.layoutIfNeeded()
			}
			anim.startAnimation()
		} else {
			self.view.layoutIfNeeded()
		}
		*/
		
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return data.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data[section].count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell")
		
		if cell == nil {
			cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
		}
		
		cell.textLabel?.text = data[indexPath.section][indexPath.row]
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return section == 0 ? "due this week" : "upcoming"
	}


}

