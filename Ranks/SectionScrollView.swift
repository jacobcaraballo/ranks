//
//  SectionScrollView.swift
//  Money
//
//  Created by Jacob Caraballo on 7/31/18.
//  Copyright © 2018 Jacob Caraballo. All rights reserved.
//

import Foundation
import UIKit


private var tableViewContentSizeDidChangeAssociationKey: UInt8 = 0
private var tableViewShouldNotifyContentChangeAssociationKey: UInt8 = 1
extension UITableView {
	
	var shouldNotifyContentChange: Bool {
		get {
			return objc_getAssociatedObject(self, &tableViewShouldNotifyContentChangeAssociationKey) as? Bool ?? true
		}
		set(newValue) {
			objc_setAssociatedObject(self, &tableViewShouldNotifyContentChangeAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}
	
	var contentSizeDidChange: ((UITableView) -> ())? {
		get {
			return objc_getAssociatedObject(self, &tableViewContentSizeDidChangeAssociationKey) as? ((UITableView) -> ())
		}
		set(newValue) {
			objc_setAssociatedObject(self, &tableViewContentSizeDidChangeAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}
	
	open override var contentSize: CGSize {
		didSet {
			if shouldNotifyContentChange {
				contentSizeDidChange?(self)
			}
		}
	}
	
}

extension UIEdgeInsets {
	
	var vertical: CGFloat {
		return top
	}
	
	var horizontal: CGFloat {
		return left
	}
	
	init(vertical: CGFloat, horizontal: CGFloat) {
		self.init(top: vertical, left: horizontal, bottom: vertical, right: horizontal)
	}
	
	init(vertical: CGFloat) {
		self.init(vertical: vertical, horizontal: -1)
	}
	
	init(horizontal: CGFloat) {
		self.init(vertical: -1, horizontal: horizontal)
	}
	
}

class SSViewSectionStyle {
	var insets: UIEdgeInsets = UIEdgeInsets(vertical: -1, horizontal: -1)
	var cornerRadius: CGFloat = -1
	var height: CGFloat = -1
	var hasBorder = false
	var hasShadow = true
	
	init() { }
	init(insets: UIEdgeInsets, cornerRadius: CGFloat) {
		self.insets = insets
		self.cornerRadius = cornerRadius
	}
	
	fileprivate func setup(withDefaultStyle style: SSViewSectionStyle) {
		
		if insets.vertical == -1 {
			insets.top = style.insets.top
			insets.bottom = style.insets.bottom
		}
		
		if insets.horizontal == -1 {
			insets.left = style.insets.left
			insets.right = style.insets.right
		}
		
		if cornerRadius == -1 {
			cornerRadius = style.cornerRadius
		}
		
		if height == -1 {
			height = style.height
		}
		
	}
}

class SSViewSection {
	private let view: UIView
	private let style: SSViewSectionStyle
	
	private var height: CGFloat {
		didSet {
			style.height = height
			heightConstraint?.constant = height
			heightChangeAction?(height)
		}
	}
	
	fileprivate var heightConstraint: NSLayoutConstraint?
	fileprivate var heightChangeAction: ((CGFloat) -> ())?
	
	init(view: UIView, style: SSViewSectionStyle) {
		self.view = view
		self.style = style
		self.height = style.height
	}
	
	func setHeight(_ height: CGFloat, animated: Bool) {
		
		self.height = height
		
		if animated {
			
			let anim = UIViewPropertyAnimator(duration: 0.2, curve: .linear) {
				self.view.frame.size.height = height
			}
			
			anim.startAnimation()
		} else {
			view.superview?.layoutIfNeeded()
		}
		
	}
}

class SectionScrollView: UIScrollView {
	
	private var styles = [UIView: SSViewSectionStyle]()
	var defaultStyle: SSViewSectionStyle
	
	init() {
		defaultStyle = SSViewSectionStyle()
		defaultStyle.height = 200
		defaultStyle.cornerRadius = 25
		defaultStyle.insets = UIEdgeInsets(vertical: 20, horizontal: 20)
		
		super.init(frame: .zero)
		alwaysBounceVertical = true
		showsVerticalScrollIndicator = false
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func willMove(toWindow newWindow: UIWindow?) {
		updateContentSize()
	}
	
	func add(_ section: UIView, height: CGFloat) -> SSViewSection {
		let s = SSViewSectionStyle()
		s.height = height
		s.cornerRadius = defaultStyle.cornerRadius
		s.insets = defaultStyle.insets
		return add(section, style: s)
	}
	
	func add(_ section: UIView, height: CGFloat, cornerRadius: CGFloat) -> SSViewSection {
		let s = SSViewSectionStyle()
		s.height = height
		s.cornerRadius = cornerRadius
		s.insets = defaultStyle.insets
		return add(section, style: s)
	}
	
	func add(_ section: UIView, height: CGFloat, insets: UIEdgeInsets) -> SSViewSection {
		let s = SSViewSectionStyle()
		s.height = height
		s.cornerRadius = defaultStyle.cornerRadius
		s.insets = insets
		return add(section, style: s)
	}
	
	func add(_ section: UIView, style: SSViewSectionStyle) -> SSViewSection {
		
		let last = subviews.last
		
		let container = UIView()
		container.clipsToBounds = true
		container.translatesAutoresizingMaskIntoConstraints = false
		addSubview(container)
		
		section.translatesAutoresizingMaskIntoConstraints = false
		container.addSubview(section)
		
		style.setup(withDefaultStyle: defaultStyle)
		let s = SSViewSection(view: section, style: style)
		s.heightConstraint = container.heightAnchor.constraint(equalToConstant: style.height)
		s.heightChangeAction = { _ in
			self.updateContentSize()
		}
		
		if style.hasBorder {
			container.setBorder(1, color: UIColor(white: 0, alpha: 0.6))
		}
		
		if style.hasShadow {
			let radius: CGFloat = 10
			container.layer.masksToBounds = false
			container.layer.shadowColor = UIColor.black.cgColor
			container.layer.shadowRadius = radius
			container.layer.shadowOffset = CGSize(width: 0, height: radius * 0.5)
			container.layer.shadowOpacity = 0.3
			container.layer.rasterize()
		}
		
		NSLayoutConstraint.activate([
			
			s.heightConstraint!,
			container.widthAnchor.constraint(equalTo: widthAnchor, constant: -(style.insets.horizontal * 2)),
			container.centerXAnchor.constraint(equalTo: centerXAnchor),
			container.topAnchor.constraint(equalTo: last?.bottomAnchor ?? topAnchor, constant: style.insets.vertical),
			
			section.heightAnchor.constraint(equalTo: container.heightAnchor),
			section.widthAnchor.constraint(equalTo: container.widthAnchor),
			section.centerXAnchor.constraint(equalTo: container.centerXAnchor),
			section.topAnchor.constraint(equalTo: container.topAnchor)
			
			])
		
		styles[container] = style
		
		return s
		
	}
	
	fileprivate func updateContentSize() {
		
		var height: CGFloat = styles.first?.value.insets.vertical ?? 0
		
		for (subview, style) in styles {
			
			subview.subviews.first?.setRadius(style.cornerRadius)
			height += style.height
			height += style.insets.vertical
			
		}
		
		contentSize.height = height
		
	}
	
}
