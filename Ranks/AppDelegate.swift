//
//  AppDelegate.swift
//  Ranks
//
//  Created by Jacob Caraballo on 8/28/18.
//  Copyright © 2018 Jacob Caraballo. All rights reserved.
//

import UIKit
import CoreData


extension CALayer {
	func rasterize() {
		self.shouldRasterize = true
		self.rasterizationScale = UIScreen.main.scale * 2.0
	}
}

extension UIView {
	func anchorToBounds(ofView view: UIView) {
		NSLayoutConstraint.activate([
			
			widthAnchor.constraint(equalTo: view.widthAnchor),
			heightAnchor.constraint(equalTo: view.heightAnchor),
			centerXAnchor.constraint(equalTo: view.centerXAnchor),
			centerYAnchor.constraint(equalTo: view.centerYAnchor)
			
			])
		
	}
	func imageRepresentation() -> UIImage {
		
		UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0)
		drawHierarchy(in: bounds, afterScreenUpdates: true)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return image!
		
	}
	func copyView<T: UIView>() -> T {
		return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
	}
	func animateByFlashingAlpha(_ from: CGFloat, to: CGFloat, numberOfTimes n: Int, intervalDuration: Double, completion: (() -> ())? = nil) {
		if n == 0 {
			completion?()
			return
		}
		
		UIView.animate(withDuration: intervalDuration, animations: {
			self.alpha = from
		}, completion: { _ in
			self.animateByFlashingAlpha(to, to: from, numberOfTimes: n - 1, intervalDuration: intervalDuration, completion: completion)
		})
	}
	func sizeToFitWidth() {
		let width = frame.size.width
		sizeToFit()
		setWidth(width)
	}
	func getCenterY(_ aligningView: UIView) -> CGFloat {
		return (aligningView.frame.size.height / 2) - (frame.size.height / 2)
	}
	func getCenterX(_ aligningView: UIView) -> CGFloat {
		return (aligningView.frame.size.width / 2) - (frame.size.width / 2)
	}
	func centerX(_ aligningView: UIView) {
		center = CGPoint(x: aligningView.frame.size.width / 2, y: center.y)
	}
	func centerY(_ aligningView: UIView) {
		center = CGPoint(x: center.x, y: aligningView.frame.size.height / 2)
	}
	func centerInView(_ aligningView: UIView) {
		centerX(aligningView)
		centerY(aligningView)
	}
	func addSubviews(_ views: UIView...) {
		for view in views {
			addSubview(view)
		}
	}
	func runBlockOnAllSubviews(_ block: (_ view: UIView) -> (Bool)) {
		if !block(self) {
			return
		}
		
		for view in subviews {
			view.runBlockOnAllSubviews(block)
		}
	}
	
	func addGradient(_ colors: [UIColor]?) {
		var gradientLayer: CAGradientLayer!
		if let layers = layer.sublayers {
			for layer in layers {
				if let gradient = layer as? CAGradientLayer, gradient.name == "gradientLayer" {
					gradientLayer = gradient
				}
			}
		}
		
		if gradientLayer != nil {
			gradientLayer.removeFromSuperlayer()
			gradientLayer = nil
		}
		
		if let colors = colors {
			gradientLayer = CAGradientLayer()
			gradientLayer.name = "gradientLayer"
			gradientLayer.frame = bounds
			//			gradientLayer.
			var c = colors.map{ $0.cgColor }
			if c.count == 1 {
				let color = colors[0]
				c = [CGColor]()
				var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
				if color.getRed(&r, green: &g, blue: &b, alpha: &a) {
					let lmod: CGFloat = 0.2
					let dmod: CGFloat = 0.2
					c.append(UIColor(red: r + lmod, green: g + lmod, blue: b + lmod, alpha: a).cgColor)
					c.append(color.cgColor)
					c.append(UIColor(red: r - dmod, green: g - dmod, blue: b - dmod, alpha: a).cgColor)
				}
			}
			
			gradientLayer.colors = c
			layer.insertSublayer(gradientLayer, at: 0)
		}
		
	}
	
	func runBlockOnAllSuperviews(_ block: (_ view: UIView) -> ()) {
		block(self)
		if let superview = self.superview {
			superview.runBlockOnAllSuperviews(block)
		}
	}
	
	struct Shadow {
		var color = UIColor.black
		var radius: CGFloat = 0.5
		var offset = CGSize(width: 0.5, height: 0.5)
		var opacity: CGFloat = 0.2
		
		init(color: UIColor = UIColor.black, opacity: CGFloat? = nil, radius: CGFloat? = nil, offset: CGSize? = nil) {
			self.color = color
			self.opacity = opacity ?? self.opacity
			self.radius = radius ?? self.radius
			self.offset = offset ?? self.offset
		}
		
		init(opacity: CGFloat, radius: CGFloat? = nil) {
			self.opacity = opacity
			self.radius = radius ?? self.radius
		}
	}
	
	func setRadius(_ radius: CGFloat) {
		self.layer.masksToBounds = true
		self.layer.cornerRadius = radius
	}
	func setBorder(_ width: CGFloat, color: UIColor) {
		self.layer.borderWidth = width
		self.layer.borderColor = color.cgColor
	}
	
	func enableTestBorder(_ color: UIColor = UIColor.red) {
		self.layer.borderWidth = 1
		self.layer.borderColor = color.cgColor
	}
	
	func setSize(_ size: CGSize) {
		self.frame = CGRect(origin: self.frame.origin, size: size)
	}
	
	func setHeight(_ height: CGFloat) {
		setSize(CGSize(width: frame.size.width, height: height))
	}
	
	func setWidth(_ width: CGFloat) {
		setSize(CGSize(width: width, height: frame.size.height))
	}
	
	func setOrigin(_ origin: CGPoint) {
		self.frame = CGRect(origin: origin, size: self.frame.size)
	}
	
	func setX(_ x: CGFloat) {
		setOrigin(CGPoint(x: x, y: frame.origin.y))
	}
	
	func setY(_ y: CGFloat) {
		setOrigin(CGPoint(x: frame.origin.x, y: y))
	}
	
	func setShadow(_ shadow: Shadow = Shadow()) {
		self.layer.shadowColor = shadow.color.cgColor
		self.layer.shadowRadius = shadow.radius
		self.layer.shadowOffset = shadow.offset
		self.layer.shadowOpacity = Float(shadow.opacity)
		self.layer.rasterize()
	}
	
	func removeShadow() {
		self.layer.shadowOpacity = 0
	}
	
	func animateScale(_ scale: CGFloat, duration: CGFloat = 0.08) {
		let anim = UIViewPropertyAnimator(duration: TimeInterval(duration), curve: .easeOut) {
			self.transform = CGAffineTransform(scaleX: scale, y: scale)
		}
		anim.startAnimation()
	}
	
	func setScale(_ scale: CGFloat, center: Bool = true) {
		transform = CGAffineTransform(scaleX: scale, y: scale)
	}
	
	func setRotation(angle: CGFloat) {
		transform = CGAffineTransform(rotationAngle: angle)
	}
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
		// Saves changes in the application's managed object context before the application terminates.
		self.saveContext()
	}

	// MARK: - Core Data stack

	lazy var persistentContainer: NSPersistentContainer = {
	    /*
	     The persistent container for the application. This implementation
	     creates and returns a container, having loaded the store for the
	     application to it. This property is optional since there are legitimate
	     error conditions that could cause the creation of the store to fail.
	    */
	    let container = NSPersistentContainer(name: "Ranks")
	    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
	        if let error = error as NSError? {
	            // Replace this implementation with code to handle the error appropriately.
	            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	             
	            /*
	             Typical reasons for an error here include:
	             * The parent directory does not exist, cannot be created, or disallows writing.
	             * The persistent store is not accessible, due to permissions or data protection when the device is locked.
	             * The device is out of space.
	             * The store could not be migrated to the current model version.
	             Check the error message to determine what the actual problem was.
	             */
	            fatalError("Unresolved error \(error), \(error.userInfo)")
	        }
	    })
	    return container
	}()

	// MARK: - Core Data Saving support

	func saveContext () {
	    let context = persistentContainer.viewContext
	    if context.hasChanges {
	        do {
	            try context.save()
	        } catch {
	            // Replace this implementation with code to handle the error appropriately.
	            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	            let nserror = error as NSError
	            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
	        }
	    }
	}

}

