//
//  JCore.swift
//  JCore
//
//  Created by Jacob Caraballo on 8/6/18.
//  Copyright © 2018 Jacob Caraballo. All rights reserved.
//

import Foundation
import CoreData



var jcore = JCore.shared
class JCore {
	
	static var shared = JCore()
	
	let container: NSPersistentContainer
	var context: NSManagedObjectContext {
		return container.viewContext
	}
	var coordinator: NSPersistentStoreCoordinator {
		return container.persistentStoreCoordinator
	}
	
	init() {
		
		
		// initialize our container
		container = NSPersistentContainer(name: "JCore")
		
		
		// load the persistent store
		container.loadPersistentStores { (description, error) in
			
			if let error = error as NSError? {
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
			
		}
		
		
		// setup context
		context.automaticallyMergesChangesFromParent = true
		
		
	}
	
	private func migrate(storeWithName name: String) {
		guard let oldURL = Bundle.main.url(forResource: name, withExtension: "momd"), let store = coordinator.persistentStore(for: oldURL) else { return }
		_ = try? coordinator.migratePersistentStore(store, to: NSPersistentContainer.defaultDirectoryURL(), options: nil, withType: NSSQLiteStoreType)
	}
	
	func save(context: NSManagedObjectContext = JCore.shared.context) {
		if context.hasChanges {
			
			do {
				try context.save()
			} catch let error as NSError {
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
			
			if context != JCore.shared.context {
				save()
			}
			
		}
	}
	
	func performBackgroundTask(_ task: @escaping (NSManagedObjectContext) -> Void) {
		container.performBackgroundTask { (context) in
			
			// perform task
			task(context)
			
			// save context
			self.save(context: context)
			
		}
	}
	

	func newBackgroundContext() -> NSManagedObjectContext {
		return container.newBackgroundContext()
	}
	
	func newTemporaryContext() -> NSManagedObjectContext {
		
		let temp = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		temp.parent = context
		temp.automaticallyMergesChangesFromParent = true
		return temp
		
	}
	
	
	func new<T: NSManagedObject>(_ type: T.Type) -> T {
		let name = NSStringFromClass(type).components(separatedBy: ".").last!
		return NSEntityDescription.insertNewObject(forEntityName: name, into: context) as! T
	}
	
	
	func remove(_ object: NSManagedObject?) {
		guard let object = object else { return }
		context.delete(object)
		save()
	}
	
	
	/**
	
	Begins a request for the every object in the database that is associated with the given type.
	
	- Parameter type: The object type that is being requested from the database
	
	- Returns: A `JCoreRequest` representing the objects for the given type.
	
	*/
	func data<T: NSManagedObject>(_ type: T.Type) -> JCoreRequest<T> {

		let className = NSStringFromClass(type).components(separatedBy: ".").last!
		let request = NSFetchRequest<T>(entityName: className)
		request.returnsObjectsAsFaults = false
		return JCoreRequest<T>(request: request)
		
	}
	
}
