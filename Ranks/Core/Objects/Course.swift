//
//  Course.swift
//  Ranks
//
//  Created by Jacob Caraballo on 8/28/18.
//  Copyright © 2018 Jacob Caraballo. All rights reserved.
//

import Foundation
import CoreData

extension Course {
	
	convenience init(name: String) {
		self.init(context: jcore.context)
		self.name = name
	}
	
}
