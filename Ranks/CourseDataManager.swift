//
//  CourseDataManager.swift
//  Ranks
//
//  Created by Jacob Caraballo on 8/28/18.
//  Copyright © 2018 Jacob Caraballo. All rights reserved.
//

import Foundation

struct CourseData {
	let acronym: String
	let section: String
	let number: String
	let title: String
	
	var string: String {
		return "\(acronym) \(number) - \(title)"
	}
	
}

class CourseDataManager {
	
	static private var _courses: [CourseData]?
	static var courses: [CourseData] {
		
		if _courses == nil {
			var courses = [CourseData]()
			let coursesPath = Bundle.main.path(forResource: "sample", ofType: nil)!
			let courseString = try! String(contentsOfFile: coursesPath);
			
			let lines = courseString.components(separatedBy: .newlines);
			
			for line in lines {
				let arr = line.components(separatedBy: " - ")
				guard arr.count == 4 else { continue }
				courses.append(CourseData(acronym: arr[0], section: arr[1], number: arr[2], title: arr[3]))
			}
			
			_courses = courses
		}
		
		return _courses!
		
	}
	
}
